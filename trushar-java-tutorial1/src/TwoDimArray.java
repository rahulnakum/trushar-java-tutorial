public class TwoDimArray {

    public static void main(String [] args){

        int [][] allMarks = {{1, 2, 3, 4, 5},{11,12,13,14,15},{21,22,23,24,25}};

        for(int row = 0 ; row <allMarks.length ; row++){
            for (int column =0;column<4;column++) {
                System.out.print(allMarks[row][column] + "\t");
            }
            System.out.println();
        }

    }
}
