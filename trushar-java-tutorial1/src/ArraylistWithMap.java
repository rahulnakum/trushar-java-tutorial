import java.util.ArrayList;
import java.util.HashMap;

public class ArraylistWithMap {
    public static void main(String[] args) {

        HashMap<String, Object> Rahul = new HashMap<String, Object>();
        Rahul.put("Name", "Rahul");
        Rahul.put("Address", "address1");
        Rahul.put("Marks", 10);

        HashMap<String, Object> Bhavisha = new HashMap<String, Object>();
        Bhavisha.put("Name", "Bhavisha");
        Bhavisha.put("Address", "address2");
        Bhavisha.put("Marks", 5);

        HashMap<String, Object> Trushar = new HashMap<String, Object>();
        Trushar.put("Name", "Trushar");
        Trushar.put("Address", "address3");
        Trushar.put("Marks", 20);

        ArrayList<HashMap<String, Object>> studentList = new ArrayList<HashMap<String, Object>>();
        studentList.add(Rahul);
        studentList.add(Bhavisha);
        studentList.add(Trushar);

        for(HashMap<String, Object> element : studentList){
            if((int)element.get("Marks") >= 10){
                System.out.println(element.get("Name"));
            }
        }

        /*
        Student rahul = new Student("Rahul", "adress1", 10);
        Student Bhavisha = new Student("Bhavisha", "adress2", 5);
        Student Trushar = new Student("Trushar", "adress3", 25);

        ArrayList<Student> studentList = new ArrayList<Student>();
        studentList.add(rahul);
        studentList.add(Bhavisha);
        studentList.add(Trushar);

        //print aall students with more than 10 marks
        System.out.println("Students with more than 10 marks");
        for(Student currentStudent : studentList){
            if(currentStudent.marks >= 10){
                System.out.println(currentStudent.name);
            }
        }*/
    }


}
