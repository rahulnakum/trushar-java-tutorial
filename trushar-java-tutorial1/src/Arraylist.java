import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class Arraylist {
    public static void main(String[] args) {
        ArrayList<Integer> marks = new ArrayList<Integer>();
        marks.add(10);
        marks.add(5);
        marks.add(25);

        ArrayList<String> Name = new ArrayList<String>();
        Name.add("Rahul");
        Name.add("Bhavisha");
        Name.add("Trushar");

        ArrayList<String> Address = new ArrayList<String>();
        Address.add("address1");
        Address.add("address2");
        Address.add("address3");

        Collections.sort(marks);


        System.out.println("Hieght marks : " + marks.get(marks.size()-1));
        System.out.println("second Hieght marks : " + marks.get(marks.size()-2));
        System.out.println("lowest marks : " + marks.get(0));

        System.out.println("======marks greater than 20");
        marks.stream().filter(x -> x > 20).forEach(System.out::println);
        System.out.println("=================");


        for (int element: marks) {
            System.out.println(element);
        }
    }

    public class Student{
        String name;
        String address;
        int marks;

        public Student(String name, String address, int marks){
            this.name = name;
            this.address = address;
            this.marks = marks;
        }
    }
}
