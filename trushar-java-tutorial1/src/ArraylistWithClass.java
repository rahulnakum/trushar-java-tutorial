import java.util.ArrayList;
import java.util.Collections;

class Student{
    String name;
    String address;
    int marks;

    public Student(String name, String address, int marks){
        this.name = name;
        this.address = address;
        this.marks = marks;
    }
}

public class ArraylistWithClass {
    public static void main(String[] args) {

        Student rahul = new Student("Rahul", "adress1", 10);
        Student Bhavisha = new Student("Bhavisha", "adress2", 5);
        Student Trushar = new Student("Trushar", "adress3", 25);

        ArrayList<Student> studentList = new ArrayList<Student>();
        studentList.add(rahul);
        studentList.add(Bhavisha);
        studentList.add(Trushar);

        //print aall students with more than 10 marks
        System.out.println("Students with more than 10 marks");
        for(Student currentStudent : studentList){
            if(currentStudent.marks >= 10){
                System.out.println(currentStudent.name);
            }
        }
    }


}
